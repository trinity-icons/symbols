const { resolve, basename, join } = require("path")

const INDEX_FILENAME      = `index.html`
const ICON_STATS_FILENAME = `icon-stats.json`

const ROOT_PATH           = resolve(__dirname, '..')
const PUBLIC_PATH         = resolve(ROOT_PATH, 'public')
const ASSETS_PATH         = resolve(ROOT_PATH, 'assets')

const INDEX_FILE_PATH     = join(PUBLIC_PATH, INDEX_FILENAME)
const BASE_PATH           = join(ASSETS_PATH, 'icons')
const LIB_PATH            = join(ASSETS_PATH, 'lib')


module.exports = {

  ICON_STATS_FILENAME,
  INDEX_FILENAME,

  INDEX_FILE_PATH,
  PUBLIC_PATH,
  ASSETS_PATH,
  ROOT_PATH,
  BASE_PATH,
  LIB_PATH,

}
